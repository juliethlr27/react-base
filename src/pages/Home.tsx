import React from "react";

const Home: React.FC = () => {

  return (
    <div
      className="Home d-flex justify-content-center align-items-center vh-100"
      data-testid="HomeComponent"
    >
      <button
        type="button"
        className="btn btn-lg btn-primary"
        data-testid="ButtonSubmit"
      >
        React Course Base
      </button>
      <div />
    </div>
  );
};

export default Home;
